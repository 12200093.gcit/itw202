import React from "react";
import {Text, View, StyleSheet} from 'react-native';
export default function TextComponent(){
    return(
        <View>
            <Text style={{padding:80, fontSize:16, marginTop:400, marginVertical:40,}}>
                The <Text style={styles.Bold}>quick brown fox </Text>
                jumps over the lazy dog</Text>
        </View>
    );
}
const styles = StyleSheet.create({
    
    Bold: {
      fontWeight: 'bold',
      color: 'black'
    }
});