import { firebase } from '@firebase/app'

const firebaseConfig = {
  apiKey: "AIzaSyAYXw6TZub2t5Nex7fJD5lLTauwYt2FdNU",
  authDomain: "fir-92666.firebaseapp.com",
  projectId: "fir-92666",
  storageBucket: "fir-92666.appspot.com",
  messagingSenderId: "517802692735",
  appId: "1:517802692735:web:0c7f9d36005fef3d11ef4a"
};
firebase.initializeApp(firebaseConfig)

export default firebase;
