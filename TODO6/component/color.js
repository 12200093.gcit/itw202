import React from"react";
import {View,StyleSheet,Text} from 'react-native';

const Color =()=> {
    return (
        <View style={styles.container}>
        
        <View style={styles.square1}>
          <Text>1</Text>
        </View>
        <View style={styles.square2}>
          <Text>2</Text></View>
        <View style={styles.square3}>
        <Text>3</Text>
        </View>
        </View>
    
      )
    }
    
    const styles = StyleSheet.create({
    
      container: {
        marginBottom:400,
        marginRight:35,
        flexDirection:"row"
      },
      square1:{
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:"red",
        width:50,
        height:200,
         
      },
      square2:{
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:"blue",
        width:120,
        height:200,
        
      },
      square3:{
        alignItems:'center',
        backgroundColor:"green",
        width:10,
        height:200,
        justifyContent:'center',
      }, 
      
    });
    
export default Color;