import { Alert, Dimensions, KeyboardAvoidingView, ScrollView, StyleSheet, Text, TextInput, useWindowDimensions, View, } from 'react-native'
import React, { useState } from 'react'
import PrimaryButton from '../component/ui/PrimaryButton'
import Colors from '../constant/Color';
import Title from '../component/ui/Title';
import InstructionText from '../component/ui/InstructionText';
import Card from '../component/ui/Card';

function StartGameScreen  ({onPickNumber}) {
    const[enteredNumber,setEnteredNumber]= useState('');
    const {width, height} = useWindowDimensions();

    function numberInputHandler(enteredText){
      setEnteredNumber(enteredText);
    }
    function resetInputHandler(){
      setEnteredNumber('')
    }

function confirmInputHandler(){
  const chosenNumber = parseInt(enteredNumber) 

  if (isNaN(chosenNumber) || chosenNumber <= 0 || chosenNumber > 99){
    
    Alert.alert('Invalid Number!', 'Number has to be a number between 1 and 99,',
    [{text:'Okay',style:'destructive', onPress:resetInputHandler }]
    )
    return;

    
  }
  // console.log(chosenNumber)
  onPickNumber(chosenNumber)

}    
 const marginTopDistance = height < 380? 30 :100;

  return (
    <ScrollView style ={styles.screen}>
    <KeyboardAvoidingView style = {styles.screen} behavior ="position">
    <View style = {[styles.rootContainer, {marginTop:marginTopDistance}]}>
      <Title>Guess My Number</Title>
      <Card style = {styles.inputContainer}>
      <InstructionText>Enter a Number</InstructionText>
    <TextInput
     style={styles.numberInput}
     keyboardType='number-pad'
     maxLength={2}
     autoCapitalize='none'
     autoCorrect={false}
     value={enteredNumber}
     onChangeText={numberInputHandler}
     />
     <View style={styles.buttonsContainer}>
       <View style={styles.buttonContainer}>
          <PrimaryButton onPress={resetInputHandler}>Reset</PrimaryButton>
       </View>
       <View style={styles.buttonContainer}>
       <PrimaryButton onPress={confirmInputHandler}>Confirm</PrimaryButton>
       </View>
     </View>
     </Card>
</View>
</KeyboardAvoidingView>
</ScrollView>
)
}
export default StartGameScreen
const deviceHeight = Dimensions.get('window').height;
const styles = StyleSheet.create({
  screen:{
    flex:1,
  },
  rootContainer:{
    flex:1,
    marginTop: deviceHeight < 380? 30:100,
    alignItems:'center'

  },
  instructionText:{
    color:Colors.accent500,
    fontSize:24

  },

inputContainer: {
    justifyContent: 'center',
    alignItems:'center',
    marginTop: 100,
    marginHorizontal: 24,
    padding: 16,
    backgroundColor: Colors.primary800,
    borderRadius: 8,
    elevation: 4,
    shadowColor: 'black',
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 6,
    shadowOpacity: 0.25
},

numberInput: {
    height: 50,
    width: 50,
    fontSize: 32,
    borderBottomColor:Colors.accent500,
    borderBottomWidth: 2,
    color: Colors.accent500,
    marginVertical: 8,
    fontWeight: 'bold',
    textAlign: 'center'
},
buttonsContainer: {
    flexDirection: 'row'
},
buttonContainer: {
    flex: 1,
}
})
