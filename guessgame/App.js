import { StatusBar } from 'expo-status-bar';
import { ImageBackground, SafeAreaView, StyleSheet,Text, View,  } from 'react-native';
import StartGameScreen from './screens/StartGameScreen';
import {LinearGradient, } from'expo-linear-gradient'
import { useState } from 'react';
import GameScreen from './screens/GameScreen'
import GameOverScreen from './screens/GameOverScreen';
import AppLoading from 'expo-app-loading';
import { useFonts } from 'expo-font';


export default function App() {
  const [userNumber, setUserNumber] = useState()
  const [gameIsOver, setGameIsOver] = useState(true)
  const[guessRounds, setGuessRounds] = useState(0)
;
 const [fontsLoaded]= useFonts({
  'open-sans': require('./assets/fonts/OpenSans-Regular.ttf'),
  'open-sans-bold': require('./assets/fonts/OpenSans-Bold.ttf'),
})
if (!fontsLoaded){
  return<AppLoading/>
}
   function pickedNumberHandler(pickerNumber) {
     setUserNumber(pickerNumber)
     setGameIsOver(false);
   }

   function gameOverHandler(numberOffRounds){
     setGameIsOver(true);
     setGuessRounds(numberOffRounds)
   }

   function startNewGameHandler(){
     setUserNumber(null);
     setGuessRounds(0);
   }

   let screen = <StartGameScreen onPickNumber = {pickedNumberHandler}/>

   if(userNumber){
     screen=<GameScreen userNumber={userNumber} onGameOver={gameOverHandler}/>
   }
   if(gameIsOver && userNumber){
     screen= <GameOverScreen
                userNumber={userNumber}
                roundsNumber={guessRounds}
                onStartNewGame={startNewGameHandler}/>
   }
  return (
    <>
    <StatusBar style='dark'/>
    <LinearGradient colors={['#185a9d','#b0bfb4']}  style={styles.container}>
      <ImageBackground
       source={require('./assets/images/bg.png')}
       resizeMode="cover"
       style={styles.rootScreen}
       imageStyle={styles.backgroundImage} >
       <SafeAreaView style = {styles.container}>
      {screen}
      </SafeAreaView>
      </ImageBackground>
    </LinearGradient></>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
 
  },
  rootScreen:{
    flex:1,
  },
  backgroundImage:{
    opacity:0.15,
  }

});
