import { StyleSheet, Text, View , Platform} from 'react-native'
import React from 'react'


function Title({children}) {
  return (
    
      <Text style={styles.title}>{children}</Text>

  )
}

export default Title;

const styles = StyleSheet.create({
    title :{
      fontFamily:'open-sans-bold',
      // borderWidth: Platform.select({IOS:0, android:2}),
        // borderWidth: Platform.OS === 'android' ? 2: 0,
        borderColor:'black',
        textAlign:'center',
        color:'black',
        fontSize:24,
        padding:12,
        maxWidth:'80%',
        width:300,
        // fontWeight:'bold'
      }
})
