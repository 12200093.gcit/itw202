import { Pressable, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import Colors from '../../constant/Color'

const PrimaryButton = ({children, onPress}) => {
  return (
        
    <View style={styles.buttonOuterContainer}>
        <Pressable android_ripple={{color:'yellow'}}
                style={({pressed}) => pressed?
            [styles.buttonInnerContainer,styles.pressed]:
                styles.buttonInnerContainer}
                onPress={onPress}>
            <Text style={styles.buttonText}>{children}</Text>
              
       </Pressable>
    </View>
       
  )
}

export default PrimaryButton

const styles = StyleSheet.create({
    buttonOuterContainer:{
        borderRadius: 28,
        margin:4,
        overflow:'hidden'
    },
    buttonText:{
        color:Colors.accent500,
        textAlign:'center',
    },
    buttonInnerContainer:{
        backgroundColor:Colors.primary600,
        paddingVertical:8,
        paddingHorizontal:16,
        elevation:2,
    },
    pressed:{
        opacity:0.75,
    }
})
