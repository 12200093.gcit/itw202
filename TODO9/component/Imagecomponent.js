import React from'react'
import {View,Image, StyleSheet,} from 'react-native';

const Imagecomponent=()=> {
    return(
        <View style={styles.container}>
            <Image style={styles.d1}
            source={require('../assets/1.png')}/>
            <Image style={styles.d2}
            source={{uri:'https://picsum.photos/100/100'}}/>
        </View>
    )
}
export default Imagecomponent

const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems:'center',
        paddingTop:300
    },
    d1:{
        width:100,height:100,
    },
    d2:{
        width:100,height:100
    },
});
