import { StyleSheet, Text, View, Image,  } from 'react-native';
import { Provider } from 'react-native-paper';
// import { BottomNavigation, Provider } from 'react-native-paper';
import { theme } from './src/theme';
import Button from './src/components/Button';
import TextInput from './src/components/Textinput';
import Header from './src/components/Header';
import {NavigationContainer} from '@react-navigation/native'
import {createStackNavigator} from '@react-navigation/stack';
import { StartScreen, LoginScreen, RegisterScreen, ResetPasswordScreen, HomeScreen, ProfileScreen, AuthLoadingScreen } from './src/Screens';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import DrawerContent from './src/components/DrawerContent';
import firebase from 'firebase/app';
import { firebaseConfig } from './src/core/config';

if (!firebase.apps.length){
  firebase.initializeApp(firebaseConfig);
}
const Stack= createStackNavigator();
const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();
export default function App() {
  return (
    <Provider theme={theme}>
      {/* <View style={styles.container}>
        <Text>Open</Text>
        <Header>Open</Header>
        <Button mode='contained'>Click me</Button>
        <TextInput label = "Email"/>
      </View> */}
      <NavigationContainer>
        <Stack.Navigator
          // initialRouteName='StarScreen'
          initialRouteName='AuthLoadingScreen'
          screenOptions= {{headerShown: false}}
        >
            <Stack.Screen name='AuthLoadingScreen' component={AuthLoadingScreen}/>
            <Stack.Screen name='StarScreen' component={StartScreen}/>
            <Stack.Screen name='LoginScreen' component={LoginScreen}/>
            <Stack.Screen name='RegisterScreen' component={RegisterScreen}/>
            <Stack.Screen name='ResetPasswordScreen' component={ResetPasswordScreen}/>
            {/* <Stack.Screen name='HomeScreen' component={BottomNavigation}/> */}
            <Stack.Screen name='HomeScreen' component={DrawerNavigator}/>
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}

function BottomNavigation(){
  return (
    <Tab.Navigator>
      <Tab.Screen 
        name='Home' 
        component={HomeScreen}
        options={{
          tabBarIcon: ({size}) => {
            return(
              <Image
                style={{width: size, height: size}}
                source={
                  require('./assets/Home-icon.png')
                }
                />
            )
          }
        }} />
        {/* <Tab.Navigator name="Home" component ={HomeScreen} /> */}
      <Tab.Screen 
        name='Profile' 
        component={ProfileScreen} 
        options={{
          tabBarIcon: ({size}) => {
            return (
              <Image
              style={{ width: size, height: size }}
              source={
                require('./assets/setting-icon.png')
              }
              />
            )
          }
        }}/>
    </Tab.Navigator>
  )
}

const DrawerNavigator = () => {
  return (
    <Drawer.Navigator drawerContent={DrawerContent}>
      <Drawer.Screen name= "HomeScreen" component={BottomNavigation} />
    </Drawer.Navigator>
  )
}

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });


