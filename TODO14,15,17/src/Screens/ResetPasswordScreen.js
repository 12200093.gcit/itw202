import React, { useState } from "react";
import {View, StyleSheet, Text} from "react-native";
import Button from "../components/Button";
import Paragraph from "../components/Paragraph";
import Background from "../components/Background";
import Header from "../components/Header";
import Logo from "../components/Logo";
import TextInput from "../components/Textinput";
import { emailValidator } from "../core/helpers/emailValidator";
import { passwordValidator } from "../core/helpers/passwordValidator";
import BackButton from "../components/BackButton";
// import { TouchableOpacity } from "react-native-gesture-handler";
// import { theme } from "../theme";

export default function ResetPasswordScreen({navigation}){
    const [email, setEmail] = useState({value:"", error: ""})
    // const [password, setPassword] = useState({value:"", error: ""})

    const onSubmitPressed = () => {
        // const emailError = email.value ? "" : "Email can't be empty.";
        const emailError = emailValidator(email.value);
        // const passwordError = passwordValidator(password.value);
        if (emailError){
            setEmail({...email, error: emailError});
            // setPassword({...password, error: passwordError});
        }
    } 
    return (
        <Background>
            <BackButton goBack={navigation.goBack} />
            <Logo/>
            <Header>Welcome</Header>
            <TextInput
                label="Email"
                value={email.value}
                error={email.error}
                errorText={email.error}
                onChangeText={(text) => setEmail ({ value: text, error: ""})}
                description="You will receive email with password reset link."
            />
            {/* <TextInput
                label="Password"
                value={password.value}
                error={password.error}
                errorText={password.error}
                onChangeText={(text) => setPassword ({ value: text, error: ""})}
                secureTextEntry
            /> */}
            <Button mode ="contained"  onPress = {onSubmitPressed} >Send Instructions</Button>
            {/* <View>
                <Text>Don't have an account?</Text>
                <TouchableOpacity onPress={() => navigation.replace("RegisterScreen")}>
                    <Text style = {styles.link }> Sign Up </Text>
                </TouchableOpacity>
            </View> */}
        </Background>
    )
}

// const styles = StyleSheet.create({
//     row: {
//         flexDirection: 'row',
//         marginTop:4,
//     },
//     link: {
//         fontWeight: 'bold',
//         color: theme.colors.primary,
//     },
// })