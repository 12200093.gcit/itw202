import React from "react";
import { View, StyleSheet } from "react-native";
import Paragraph from "../components/Paragraph";
import Background from "../components/Background";
import Button from "../components/Button";
import Header from "../components/Header";
import Logo from "../components/Logo";

export default function StartScreen({navigation}) {
    return (
        // <View style={ styles.container }>
        //     <Header>Login Template</Header>
        //     <Paragraph>
        //         The easiest way to start with your amizing application.
        //     </Paragraph>
        //     <Button mode="outlined"> Login </Button>
        // </View>
        <Background>
            <Logo />
            <Header>Login Template</Header>
            <Paragraph>
                The easiest way to start with your amizing application.
            </Paragraph>
            <Button 
            mode="outlined"
            onPress = {() => {
                navigation.navigate("LoginScreen")
            }}
            > Login </Button>

            <Button 
            mode="outlined"
            onPress = {() => {
                navigation.navigate("RegisterScreen")
            }}
            > Sign Up </Button>
            {/* <Button mode="contained"> Sign Up </Button> */}
        </Background>
        
    )
}
// const styles = StyleSheet.create({
//     container: {
//         flex: 1,
//         backgroundColor: "#fff",
//         alignItems: "center",
//         justifyContent: "center",
//         width: "100%",
//     }
// })