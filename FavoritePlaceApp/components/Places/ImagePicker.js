import {Button, View, StyleSheet,Text, Image} from 'react-native';
import {
    launchCameraAsync,
    useCameraPermissions,
    PermissionStatus,
} from 'expo-image-picker';
import { useState } from 'react';
import { Colors } from '../../constants/Colors';
import OutlinedButton from '../UI/OutlinedButton';


function ImagePicker() {
    const [pickedImage, setpickedImage] = useState();
    const [CameraPermissionInformation, requestPermission ] = useCameraPermissions();
    async function verifyPermissions(){
        if (CameraPermissionInformation.status ===PermissionStatus.UNDETERMINED){
            const permissionResponse = await requestPermission();

            return permissionResponse.granted;
        }
        if (CameraPermissionInformation.status === PermissionStatus.DENIED){
            alert.alert(
                'Insufficient Permission!',
                'You need to grant camera permissions to use this app.'
            );
            return false;
        }
        return true;
    } 

    async function takeImageHandler(){
        const hasPermission = await verifyPermissions();

        if (!hasPermission) {
            return;
        }
        const image =await launchCameraAsync({
            allowsEditing: true,
            aspect: [16, 9],
            quality: 0.5,
        });
         // console.log(image);
        setpickedImage(image.uri);
    }

    async function takeImageHandler(){
        const image = await launchCameraAsync({
            allowsEditing: true,
            aspect: [16, 9],
            quality: 0.5,
        });
        // console.log(image);
        setpickedImage(image.uri);
    }
    let imagePreview = <Text>No image taken yet</Text>;

    if (pickedImage) {
        imagePreview = <Image style={styles.image} source= {{ uri: pickedImage}} />;
    }

    return (
        <View>
            <View style={styles.imagePreview}>{imagePreview}</View>
            {/* <Button title="Take Image" onPress={takeImageHandler}/> */}
            <OutlinedButton icon='camera' onPress={takeImageHandler}> Take Image</OutlinedButton>
        </View>
    );
}
export default ImagePicker;

const styles = StyleSheet.create({
    imagePreview: {
        width: '100%',
        height: 200,
        marginVertical: 8,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.primary100,
        borderRadius: 4,
    },
    image: {
        width: '100%',
        height: '100%',
    },
});