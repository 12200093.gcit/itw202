import { StyleSheet, Text, View, FlatList } from 'react-native'
import React from 'react'
// import PlaceItem from '../PlaceItem'
import PlaceItem from './PlaceItem'
// import { Colors } from 'react-native/Libraries/NewAppScreen';
import { Colors } from '../../constants/Colors';

const PlacesList = ({places}) => {
    if(!places || places.length === 0){
        return (<View style ={styles.fallbackContainer}>
            <Text style={styles.fallbackText}>No places added yet - start adding some!</Text>
        </View>
        );
    }
  return (
    // <View>
    //   <Text>PlacesList</Text>
    // </View>
    <FlatList
        data = {places}
        keyExtractor = {(item) => item.id}
        rederItem={({item}) => <PlaceItem place = {item}/>}
    />    
  )
}

export default PlacesList

const styles = StyleSheet.create({
    fallbackContainer:{
        flex: 1,
        justifyContent:'center',
        alignItems:'center',
    },
    fallbackText: {
        fontSize: 16,
        color: Colors.primary200
    }
})