import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import {add, multiply} from './components/Named_Exports';
import division from './components/Named_Exports';
import Courses from './components/funcComponent';
export default function App() {
  return (
    <View style={styles.container}>
      <Text>Result of addition : {add( 4,4)}</Text>
      <Text>Result of multiply : {multiply( 4,4)}</Text>
      <Text>Result of divide : {division( 4,4)}</Text>
      <Courses></Courses>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
