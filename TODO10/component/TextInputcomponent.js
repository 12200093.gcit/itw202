import React,{useState} from 'react'
import { TextInput, View , Text} from 'react-native';
const TextInputcomponent =() =>{
    const[text,setText]= useState('');
    return(
        <View style={{padding:50, }}>
            <Text>What is your name?</Text>
            <Text style={{padding: 10, fontSize: 16}}>
        {text.split(' ').join(' ')}
      </Text>
            
            <TextInput style={{borderColor:'black',borderWidth:1, width:250}}
            secureTextEntry={true} 
             onChangeText={newText => setText(newText)}
           /></View>
    );
}
export default TextInputcomponent
