import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import { Component } from 'react';
import Viewcomponent from './components/Viewcomponent';
export default function App() {
  return (
    <View style={styles.container}>
     
      <StatusBar style="auto" />
      <Viewcomponent/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 2,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

